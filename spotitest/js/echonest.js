function check_response(data) {
    if (data.response) {
        if (data.response.status.code == 5)
            console.log("No such track found...")
        else if (data.response.status.code != 0) {
            console.log("The server has other things to do, probably. Whatever.");
            console.log(data);
        } else {
            return true;
        }
    } else {
        console.log("Unexpected response from server");
    }
    return false;
}

function get_track_meta(trackid)
{
    trackid = trackid.replace('spotify', 'spotify-WW')
    var url = 'http://developer.echonest.com/api/v4/track/profile?api_key=KL9ORZ8ADN2NFYSGZ';
    var data =  { 'id': trackid,
                  'format':'json',
                  'bucket': ['audio_summary']}
    var result = undefined;
    $.ajax({url: url,
            dataType: 'json',
            async: false,
            data: data,
            success: function(data) {
                if (check_response(data))
                {
                    aresult = {}
                    var track = data.response.track;
                    aresult.artist = track.artist;
                    aresult.title = track.title;
                    aresult.id = track.foreign_id.replace("-WW", "")
                    aresult.bpm = track.audio_summary.tempo;
                    aresult.dance = track.audio_summary.danceability;
                    aresult.duration = track.audio_summary.duration;
                    aresult.energy = track.audio_summary.energy;
                    aresult.loudness = track.audio_summary.loudness;
                    aresult.key = track.audio_summary.key;
                    aresult.mode = track.audio_summary.mode;
                    console.log(aresult)
                    result = aresult;
                }
                else
                    console.log("Failed to fetch meta data for track = " + trackid);
            }});
    return result;
}