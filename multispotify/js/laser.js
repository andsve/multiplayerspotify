Laser = function (id, params, state, geometry) {
	this.geometry 	= geometry;
	this.type       = "laser";
	this.transform 	= mat4.identity();

	params = params || {
		 velocity : 600	// constant velocity
		,ttl : 1		// time-to-live / distance?!
		,radius : 2.0	// bounding sphere radius
	};

	state = state || {
		 pos : vec3.create()  	// Position
		,o : mat4.create() 		// orientation
	};

	// console.log("creating laser, track",params.track);

	state.shootTime = 0.0;
	var heading = vec3.create([state.o[8],state.o[9],state.o[10]]);
	vec3.normalize(heading);
	vec3.scale(heading,20);
	vec3.add(state.pos,heading);

	GameObject.call( this, id, params, state, geometry.radius * 1.25 );
}

Laser.prototype = new GameObject();
Laser.prototype.constructor = Laser;

Laser.prototype.Update = function(dt) {
	this.state.shootTime += dt;

	if (this.state.shootTime >= this.params.ttl) {
		return false;
	}

	// update position based on velocity
	// pos = pos0 + dir * velocity * dt
	var dir = vec3.create([this.state.o[8],this.state.o[9],this.state.o[10]]);
	vec3.normalize(dir);

	var tmp = vec3.create();
	vec3.scale(dir,this.params.velocity * dt,tmp);
	vec3.add(this.state.pos,tmp,this.state.pos);
	

	mat4.identity(this.transform);
	
	mat4.translate(this.transform,this.state.pos,this.transform);
	mat4.multiply(this.transform,this.state.o,this.transform);
	mat4.scale(this.transform,[this.params.radius, this.params.radius, this.params.radius ],this.transform);


	return true;
}

Laser.prototype.Render = function(shader,bindBuffers) {
	shader.SetUniform("mat4", "uOBJMatrix", this.transform);
	shader.SetUniform("f", "lazer_time", this.state.shootTime / this.params.ttl);
	shader.SetUniform("f3", "color", this.params.drawColor);
	shader.SetUniform("i", "state", 1);


	this.geometry.BindBuffers(shader, bindBuffers);
	this.geometry.DrawBuffers();
	this.geometry.UnbindBuffers(shader,bindBuffers);
}