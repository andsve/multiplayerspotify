var game_elems = {};
var ctx;
var scene;
var ani = 0;

var inLimbo = false;
var timeInLimbo = 0.0;

// Totally cheating
var playerColors = {};

var fragLog = [];

// geometry stuff
var shipGeos = [];
var debrisGeos = {};
var starsGeo;
var starsTexture;

var gamestate;
var asteroid_count = 30;

var martin_timell = new Timer(); // martin_timell tells the time
var game_timer = new Timer();
game_timer.start();

// suicide
PXF.registerKeyCallback("k", function() {
	socket.emit("destroy", { "type" : "player", "id" : gamestate.localPlayerObject.id });
});

function joinGame (room) {

	game_elems["holder"] = document.getElementById('game');
	game_elems["glcontext"] = document.getElementById('gamecanvas');
	game_elems["holder"].style.display = "block";

	// Try to join specific game name
	socket.emit("join", room);

	// populate playlist-thing
	game_elems["ammo_list"] = document.getElementById("ammo_list");
	if (game_elems["ammo_list"]) {
		var i = 0;
		ammo_list.forEach(function(track) {
			var row = document.createElement("tr");
			var td = document.createElement("td");
			td.textContent = track.trackname;
			row.id = "track-" + String(i);
			track.i = i++;

			row.appendChild(td);
			game_elems["ammo_list"].appendChild(row);
		});

		var lols = document.getElementById("track-0");
		lols.style.backgroundColor = "green";
	}
	
	// Clear game state
	game_elems["glcontext"].width = w;
	game_elems["glcontext"].height = h;
	ctx = new PXF.Instance( w, h, game_elems["glcontext"], true, true, false, true, true );
	gl = ctx.gl;

	// load base player texture
	playerTexture = new PXF.Texture( gl, "data/colors_base.png", {"magFilter" : gl.NEAREST, "minFilter" : gl.NEAREST});
	asteroidTexture = new PXF.Texture( gl, "data/asteroid.png", {"magFilter" : gl.NEAREST, "minFilter" : gl.NEAREST});

	// load ship geometry
	meshLoader = new PXF.MeshLoader( gl );
	shipGeos.push( meshLoader.Load("data/ship.mesh") );

	// calculate bounding sphere for all 
	for(var m in shipGeos) {
		calculateBoundingSphere(shipGeos[m]);
	}

	// load stars geometry and texture
	starsGeo 		= meshLoader.Load("data/stars.mesh");
	starsTexture 	= new PXF.Texture( gl, "data/stars.png", {"magFilter" : gl.NEAREST, "minFilter" : gl.NEAREST});

	// create laser geometry
	laserMesh = meshLoader.Load("data/laserCube.mesh");
	calculateBoundingSphere(laserMesh);

	// 
	var asteroidGeo = meshLoader.Load("data/asteroid1.mesh");
	calculateBoundingSphere(asteroidGeo);

	// debris geos
	debrisGeos["leftwing"] = meshLoader.Load("data/left_wing.mesh");
	debrisGeos["shipbody"] = meshLoader.Load("data/ship_body.mesh");
	debrisGeos["asteroid"] = asteroidGeo;

	// set gl states
	gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);//gl.ONE_MINUS_DST_ALPHA,gl.DST_ALPHA);
}

function resizeCanvas(event) {
	//(window.innerWidth, window.innerHeight);
	w = window.innerWidth;
	//w = clamp(w, 0, 1024);
	h = window.innerHeight - 60;
	(document.getElementsByTagName("body")[0]).style.overflow = "hidden";
	//h = clamp(h, 0, 768);
	game_elems["glcontext"].width = w;
	game_elems["glcontext"].height = h;
	game_elems["glcontext"].style.width = w + "px";
  	game_elems["glcontext"].style.height = h + "px";
	camera.Perspective( 60.0, w/h, 0.1, 1000.0 );
}

function calculateBoundingSphere(geo,scale) {
	var verts = geo.buffers["position"].data;
	if(!verts) { 
		LOG("No vertices found for bounding sphere calculationg");
		return;
	}

	var BIG = 999999999;
	var minPoint = [ BIG, BIG, BIG];
	var maxPoint = [-BIG,-BIG,-BIG];

	var numVerts = verts.length / 3;

	for(var v = 0; v < numVerts; v++) {
		var x,y,z;

		var index = v * 3;

		x = verts[index];
		y = verts[index+1];
		z = verts[index+2];

		minPoint[0] = Math.min(x,minPoint[0]);
		minPoint[1] = Math.min(y,minPoint[1]);
		minPoint[2] = Math.min(z,minPoint[2]);

		maxPoint[0] = Math.max(x,maxPoint[0]);
		maxPoint[1] = Math.max(y,maxPoint[1]);
		maxPoint[2] = Math.max(z,maxPoint[2]);
	}

	var c = vec3.create();
	vec3.subtract(maxPoint,minPoint,c);
	vec3.scale(c,0.5);
	var r = vec3.length(c);

	vec3.add(minPoint,c,c);

	LOG(r,minPoint, maxPoint);

	geo.minPoint = minPoint;
	geo.maxPoint = maxPoint;

	geo.center = c;
	geo.radius = r;
}


// Handles major game session events
// such as when we/someone joined, or we got kicked.
function onGame(data) {
	if (data.event == "kicked")
	{
		lobby_elems["lobbyinfo"].innerHTML = "Kicked! " + data.reason;
		destroyGame();
		showLobby();
	} else if (data.event == "UHOST") {
		LOG("IM A HOST");

		// create a bunch of asteroids
		var max_distance = 450;
		var max_scale = 0.45;
		var min_scale = 0.22;

		for(var a = 0; a < asteroid_count; a++) {
			var dirX = Math.random();
			var dirY = Math.random();
			var dirZ = Math.random();

			dirX = dirX > 0.5 ? (dirX - 0.5) * 2.0 : -dirX * 2.0;
			dirY = dirY > 0.5 ? (dirY - 0.5) * 2.0 : -dirY * 2.0;
			dirZ = dirZ > 0.5 ? (dirZ - 0.5) * 2.0 : -dirZ * 2.0;

			var pos = vec3.create( [max_distance * dirX, max_distance * dirY, 0 ]  );


			var scaleX = (Math.random() + min_scale) * max_scale;
			var scaleY = (Math.random() + min_scale) * max_scale;
			var scaleZ = (Math.random() + min_scale) * max_scale;

			var scale = vec3.create( [scaleX,scaleX,scaleX] );

			socket.emit("spawn", 
				{ "type" : "debris", "keep" : true, "id" : "asteroid" + a, 
					"params" : { 
						"type" : "asteroid",
						"drawColor": [1,1,1],
						"drawState": 5 , 
						"drawScale": scale} ,
					"state" : {
						"pos" : pos,
						"o"   : rand_orientation(),
						"vrot" : rand_vec(Math.PI * 0.5),
						"v" : [0,0,0]
					}
				});
		}

	} else if (data.event == "welcome")
	{
		hideLobby();
		initGame();
	} else if (data.event == "playerid")
	{
		
		// TODO set to correct playerid inside a game state
		//playerid = data.playerid;
		gamestate.localplayer = data.playerid;

	} else if (data.event == "scoreboard")
	{
		var scoreboard = data.scoreboard;
		var scorediv = document.getElementById("scoreboard");
		var color;
		scorediv.innerHTML = "";
		for (var p in scoreboard) {
			color = playerColors[p];
			uname = p;//gamestate.playerObjects[p].state.username;
			if (p in gamestate.playerObjects && gamestate.playerObjects[p].state != undefined && gamestate.playerObjects[p].state.username)
			{
				uname = gamestate.playerObjects[p].state.username;
			}
			scorediv.innerHTML += "<span style=\"color: rgb("+ Math.floor(color[0]*255) + ", " + Math.floor(color[1]*255) + ", "+ Math.floor(color[2]*255) + ");\">" + uname + "</span>" + ": " + scoreboard[p]["kills"] + " kills, " + scoreboard[p]["deaths"] + " deaths<br>";
		}
	} else {
		// ("Recieved unknown game packet: ", data);
	}
}

// Handles game state changes, such as new positions etc
function onState (data) {
	if (data.type == "player" && data.id && data.id in gamestate.playerObjects)
	{
		gamestate.playerObjects[data.id].state = data.state;
	} 
}

function onDestroy(data) {
	if(data.type == "player") {
		var id = data.id;

		if (data.killer) {
			//setActiveObject(data.killer);

			if ( data.track ) {

				var entry = {
					color : gamestate.playerObjects[data.killer].params.drawColor,
					time : game_timer.seconds(),
					log : gamestate.playerObjects[data.killer].state.username + " -> " + 
						gamestate.playerObjects[data.id].state.username + " (" + data.track.trackname + ")"
				}

				fragLog.push( entry );
			}

		} else {
			if (data.id != undefined && data.id in gamestate.playerObjects )	
			{

				var entry = {
					color : gamestate.playerObjects[id].params.drawColor,
					time : game_timer.seconds(),
					log : gamestate.playerObjects[data.id].state.username + " took hens own life"
				}

				fragLog.push( entry );
			}
		}

		if (id == gamestate.localplayer) {
			gamestate.localPlayerObject.SpawnDebris();
			inLimbo = true;
			delete gamestate.playerObjects[id];
			gamestate.localPlayerLasers = {};

			// add killing track to playlist
			// TODO

		} else if(id in gamestate.playerObjects) {
			delete gamestate.playerObjects[id];
		}
	} else if (data.type == "laser") {
		var id = data.id;
		if(id in gamestate.gameObjects)
			delete gamestate.gameObjects[id];
	}
}

// Spawns game objects on requests from the server
function onSpawn(data) {
	if (data.type == "laser") {
		var laser = new Laser(data.id, data.params, data.state, laserMesh );

		if(data.params.owner == gamestate.localplayer)
			gamestate.localPlayerLasers[data.id] = laser;
		else
			gamestate.gameObjects[data.id] = laser;
	}
	else if (data.type == "player")
	{
		// create new player object based on server spawn info
		var player = new Player(data.playerid, data.params, data.state, shipGeos[0]);
		gamestate.playerObjects[data.playerid] = player; 

		// add mesh to scene for matrix updates
		scene.Add(player.mesh);

		// start rendering when we have spawned a player
		scene.init = true;

		if(player.id == gamestate.localplayer) {
			setActiveObject(player.id);
			gamestate.localPlayerObject = player;
			player.localPlayer = true;
			player.state.username = username;

			inLimbo = false;
			timeInLimbo = 0;
		}

		// Color cheat
		playerColors[player.id] = data.params.drawColor;

		// TODO create a Player-object here with the state
		//players[data.playerid] = data.state;
	} else if (data.type == "debris")
	{
		var geolol = debrisGeos[data.params.type];

		var deb = new Debris(data.id, data.params, data.state, geolol);
		gamestate.gameObjects[data.id] = deb; 

		// add mesh to scene for matrix updates
		scene.Add(deb.mesh);

	} 
}

// Destroys the webgl context etc and "hides" the game
function destroyGame () {
	game_elems["holder"].innerHTML = "";
	game_elems["holder"].style.display = "none";
	run = false;
}

// When we have successfully joined a game;
// setup game world locally and start rendering
function initGame() {
	//game_elems["holder"].innerHTML = "This is where the game is supposed to be!";
	
	gl.lineWidth(5.0);

	run = true;
	shader = new PXF.Shader( gl, "data/shader.vs", "data/shader.fs", true );
	camera = new PXF.Camera();
	orthoCamera = new PXF.Camera();
	scene 	= new PXF.Scene(ctx);

	camera.Perspective( 120.0, w/h, 0.1, 1000.0 );
	camera.position = [ 0.0, -6.0, 0.0 ];

	//camera.Update(1.0);

	camera.yaw   = 0.0;
    camera.pitch = 20.0;
    camera.zoom  = 25.0;

	camera.UpdateMatricesFromLookAt( [ 0.0, -20.0, 0.0 ] );



	orthoCamera.Ortho(-1, 1, -1, 1, 0.1, 100.0);
	// Init "previous" camera positions
	//for (var i=0; i<4; i++) {
	//	// TODO find real camera D:
	//	prevCamera.unshift(mat4.identity());
	//	//mat4.toRotationMat(camera.Transform, prevCamera[i]);
	//	//mat4.translate(camera.Transform, camera.position);
	//}

	resizeCanvas();
	window.addEventListener("resize", resizeCanvas);
	requestAnimationFrame(gameloop);

	activeObject = undefined;
	gamestate = new GameState(gl);
	martin_timell.start();

	// create bound sphere geometry
	gamestate.sphereMesh = meshLoader.Load("data/sphere.mesh");
}

function setActiveObject(id) {
	if(!(id in gamestate.playerObjects)) {
		LOG(id + " not in gamestate or is not local player"); 
		return;
	}

	if(activeObject)
		activeObject.mesh.Remove("camera");

	// set new active object based on id
	activeObject = gamestate.playerObjects[id];	
	activeObject.mesh.Add("camera",camera);

	return activeObject;
}

function updateFragLog() {
	var id = document.getElementById("fraglog");
	id.innerHTML = "";

	var time = game_timer.seconds();
	var num_logs = fragLog.length;

	var player = gamestate.localPlayerObject;

	for(var i = 0; i < num_logs; i++) {
		if ( fragLog[i] == undefined ) continue;

		var timediff = (time - fragLog[i].time);

		if ( timediff > 3.5) {
			fragLog[i] = undefined;
		} else {
			var color = fragLog[i].color;
			var rgb_string = "rgb("+ Math.floor(color[0]*255) + ", " + Math.floor(color[1]*255) + ", "+ Math.floor(color[2]*255) + ");";
			var opacity = 1.0 - timediff / 3.5;

			var html_str = "<h style='opacity: " + opacity + "; color: " + rgb_string + "'>" + fragLog[i].log + "</h><br>";
			id.innerHTML += html_str;	
		}
	}
}

function gameloop()
{
	Input.Update();


	if (run && scene.init)
	{
		ani += 0.1;
		ctx.Update(1.0);

		gamestate.Update(martin_timell.seconds());
		ctx.UpdateObjects(scene, martin_timell.seconds());

		if (inLimbo) {
			// Player is dead
			timeInLimbo += martin_timell.seconds();

			if (timeInLimbo >= 3) {
				  var state = {   pos : [0.0, 50.0, 0.0] // Position
			         	, v : 100.0               // Heading velocity
        				, a : 30.0              // Heading acceleration
				, o : [1, 0, 0, 0, 0, 0.0, -1, 0, 0, 1, 0.0, 0, 0, 0, 0, 1] // -90° around x
        , vrot : [0.0, 0.0, 0.0] // Rotational velocity
        , drot : [0.0, 0.0, 0.0] // Rotational acceleration

        // State
				, health : 5.0
        , rolling : "STRAIGHT" // LEFT / RIGHT / STRAIGHT  - If ship is rolling
        , shooting: false
        , shootTime : 0.0  // Time since last shot
        , boosting  : false
				, boostEnergy : 3.0
				, boostCooldown : 0.0
              };

				  state.pos[0] = Math.random() * 400.0;
				  state.pos[1] = Math.random() * 400.0;
				  //state.pos[2] = Math.random() * 400.0;

				socket.emit("spawn", {"type"     : "player"
						            , "playerid" : gamestate.localplayer
					                , "params" : gamestate.localPlayerObject.params
					                , "state"  : state});
			}
		} else if ("localPlayerObject" in gamestate && "state" in gamestate.localPlayerObject && "pos" in gamestate.localPlayerObject.state)
		{
			var c = mat4.create();
			c = mat4.identity();
			mat4.rotateX(c, -1.5);

			// Relative camera
			var m1 = mat4.identity();
			mat4.rotateY(m1, 3.1415926);
			mat4.rotateX(m1, -1.5); // Look down on player :3
			mat4.translate(m1, [0.0, 0.0, 240.0]);

			// Camera position
			var m2 = mat4.identity();
			mat4.translate(m2, gamestate.localPlayerObject.state.pos);

			var cam = mat4.identity();
			mat4.multiply(cam, m2);
			mat4.multiply(cam, c);
			mat4.multiply(cam, m1);
		

			camera.WorldTransform = cam;
			mat4.inverse(camera.WorldTransform, camera.InvWorldTransform);
			// --------------------------------------------------------------------


		}

        martin_timell.stop();
        martin_timell.start();

        updateFragLog();

		render();

    }

    requestAnimationFrame(gameloop);
}


function render()
{
	var uNMatrix = mat4.create(camera.WorldTransform);
	mat4.inverse(uNMatrix);
	mat4.transpose(uNMatrix);

	// gl stuff
	gl.viewport(0,0,w,h);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// render environment / cubemap
	shader.Bind();

	var cubemap_mat = mat4.create(camera.WorldTransform);
	var cb_mat2 = mat4.create();
	mat4.identity(cb_mat2);

	mat4.translate(cb_mat2, [cubemap_mat[12], cubemap_mat[13], cubemap_mat[14]], cb_mat2);

	shader.SetUniform("mat4","uMVMatrix", camera.InvWorldTransform);
	shader.SetUniform("mat4","uPMatrix",camera.pMatrix);
	shader.SetUniform("mat4","uNMatrix",uNMatrix); 	
	shader.SetUniform("mat4","uOBJMatrix", cb_mat2);

	// Render stars
	starsTexture.Bind(shader,"tex0",0);
	gl.disable(gl.DEPTH_TEST);
	starsGeo.BindBuffers(shader, {position : true, normal : false, uv : true});
	starsGeo.DrawBuffers(shader);
	starsGeo.UnbindBuffers(shader, {position : true, normal : false, uv : true});
	gl.enable(gl.DEPTH_TEST);

	// render players
	playerTexture.Bind(shader,"tex0",0);
	asteroidTexture.Bind(shader,"tex1",1);

	gamestate.Render(shader);

	shader.Unbind();

	// post-process
	// Meh..	
}
