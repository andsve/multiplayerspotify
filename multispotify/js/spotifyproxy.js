var __trackCounter = 0;
var __artistCounter = 0;

var __getModuleRequire = function ( module ) {
	var module = {
		player : {
			play : function( track ) {

			}
		},

		Track : {
			fromURI : function( uri, cb ) {
				var track = {
					uri : uri,
					trackname : "Awesome Spotify Track " + (++__trackCounter)
				}

				cb && cb(track);

				return track;
			}
		},

		Artist : {
			fromURI : function( uri, cb ) {
				var artist = {
					uri : uri,
					name : "Awesome Spotify Artist " + (++__artistCounter)
				}

				cb && cb(artist);

				return artist;
			}
		}, 

		Playlist : {
			fromURI : function( uri, cb ) {
				var pl = {
					uri : uri,
					name : "Awesome Spotify Playlist"

				};

				cb && cb(pl);

				return pl;
			},
			add : function( track ) {
				// im a dummy
			}
		}
	};

	return module;
};

__getSpotifyApi = function() {
	return {
		require : __getModuleRequire
	}
}

getSpotifyApi = __getSpotifyApi;

username = "Spotify User";