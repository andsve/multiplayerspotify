GameState = function(gl) {
	this.gl 			= gl;
	this.playerObjects 	= {};
	this.gameObjects 	= {};
	this.localPlayerObject = {};
	this.localPlayerLasers = {};
	this.staticObjects 	= [];

	this.localplayer 	= -1;
	this.temp_tick 		= 0;
}

GameState.prototype.Update = function(dt) {
	var localPlayerObject;
	for(var p in this.playerObjects) {

		if (p == this.localplayer)
		{
			this.playerObjects[p].Update(dt, Input.state);

			this.temp_tick += 1;

			if (this.temp_tick >= 5.0)
			{
				this.playerObjects[p].SendState(socket);
				this.temp_tick = 0;
			}

			localPlayerObject = this.playerObjects[p];

			
		} else {
			this.playerObjects[p].Update(dt);
		}
	}

	// update other objects
	for(var o in this.gameObjects) {
		var object = this.gameObjects[o];

		if(!object.Update(dt)) {
			delete this.gameObjects[o];
		}
	}


	if(!localPlayerObject) return;

	// find all collision pairs this frame for player object
	var frameCollisions = [];
	this.staticObjects = [];

	var playerGeometryPosition = localPlayerObject.mesh.geometry.center;
	var playerGeometryRadius = localPlayerObject.mesh.geometry.radius;
	var playerPosition = localPlayerObject.mesh.worldPosition;

	vec3.add(playerPosition, playerGeometryPosition, playerPosition);

	// check collision with other players
	for(var p in this.playerObjects) {
		// check collision with player
		var player = this.playerObjects[p];

		if(player.id == this.localplayer || player.type != "player")
			continue;

		var geometryCenter = player.mesh.geometry.center;
		var geometryRadius = player.mesh.geometry.radius;
		var worldPosition  = player.state.pos;

		var objectPosition = vec3.create();
		vec3.add(worldPosition,geometryCenter,objectPosition);


		if(sphereCollision(playerPosition,objectPosition,playerGeometryRadius,geometryRadius)) {
			frameCollisions[p] = player.type;
		} 
	}

	// check collision with other objects
	for(var o in this.gameObjects) {
		var object = this.gameObjects[o];
		var type = object.params.type;

		if(type == "shipbody" || type == "leftwing") continue;

		if(object.player == this.localplayer) {
			continue;
		}
			
		var r = object.params.radius ? object.params.radius : object.geometry.radius;
		var p = object.state.pos;

		if(object.params.drawScale) {

			var scale = object.params.drawScale;
			var minPoint = vec3.create(object.geometry.minPoint);
			var maxPoint = vec3.create(object.geometry.maxPoint);

			vec3.multiply(minPoint,scale,minPoint);
			vec3.multiply(maxPoint,scale,maxPoint);

			var c = vec3.create();
			vec3.subtract(maxPoint,minPoint,c);
			vec3.scale(c,0.5);
			r = vec3.length(c);
			r *= 0.5;

			//object.type = "asteroid";
			object.minPoint = minPoint;
			object.maxPoint = maxPoint;
			object.radius = r;
			object.center = c;
		}

		if(sphereCollision(playerPosition,p,playerGeometryRadius,r)) {
			frameCollisions[o] = object.type;
		} else if (object.params.type == "asteroid") {
			this.staticObjects.push(object);
		}
	}

	// update lasers DONT LOOK AT ME
	for(var l in this.localPlayerLasers) {
		var laser = this.localPlayerLasers[l];

		var laser_radius = laser.params.radius ? laser.params.radius : laser.geometry.radius;
		var laser_position = laser.state.pos;

		if(!laser.Update(dt)) {
			delete this.localPlayerLasers[l];
		}

		// check collision against all "static objects"
		for(var s in this.staticObjects) {
			var object = this.staticObjects[s];

			var r = object.params.radius ? object.params.radius : object.geometry.radius;
			var p = object.state.pos;

			if(object.params.drawScale) {

				var scale = object.params.drawScale;
				var minPoint = vec3.create(object.geometry.minPoint);
				var maxPoint = vec3.create(object.geometry.maxPoint);

				vec3.multiply(minPoint,scale,minPoint);
				vec3.multiply(maxPoint,scale,maxPoint);

				var c = vec3.create();
				vec3.subtract(maxPoint,minPoint,c);
				vec3.scale(c,0.5);
				r = vec3.length(c);
				r *= 0.5;

			}

			if(sphereCollision(laser_position,p,laser_radius,r)) {
				frameCollisions[object.id] = object.params.type;
			}
		}
	}

	for(var c in frameCollisions) {
		var collision = frameCollisions[c];

		//LOG(c,collision);

		if (collision == "player")
		{
			if(!localPlayerObject.invincible)
			{
				socket.emit("destroy", { "type" : collision, "id" : c});
				localPlayerObject.Destroy();
			}
		}
		else if (collision == "laser") {

			if (!localPlayerObject.invincible)
			{
				var laser = this.gameObjects[c];
				localPlayerObject.Hit(laser.params.radius, laser.params.owner, laser.params.track );
				socket.emit("destroy", { "type" : collision, "id" : c});
			}

		} else if (collision == "debris") {
			if (!localPlayerObject.invincible)
			{
			localPlayerObject.Destroy();	
			}
		}
	}
}

function sphereCollision(p0,p1,r0,r1) {
	var distanceVec = vec3.create();
	vec3.subtract(p1,p0,distanceVec);
	var distance = vec3.length(distanceVec);
	var radiusSum = r0 + r1;

	return distance - radiusSum < 0 ? 1 : 0;
}

GameState.prototype.Render = function(shader,bindBuffers) {
	bindBuffers = { "tex0" : false };

	for(var p in this.playerObjects) {

		this.playerObjects[p].Render(shader, {position : true, normal : false, uv: true});
	}

	this.gl.enable(this.gl.BLEND);

	// draw other game objects
	shader.SetUniform("i", "state", 1); 
	for(var o in this.gameObjects) {
		this.gameObjects[o].Render(shader, {position:true, normal:false, uv:false});
	}

	for(var l in this.localPlayerLasers) {
		var laser = this.localPlayerLasers[l];
		laser.Render(shader);
	}

	for(var p in this.playerObjects) {
		this.playerObjects[p].drawEngine(shader);
		this.playerObjects[p].drawTrail(shader);
	}

	shader.SetUniform("i", "state", 0); 

	this.gl.disable(this.gl.BLEND);
	this.gl.enable(this.gl.DEPTH_TEST);
}
