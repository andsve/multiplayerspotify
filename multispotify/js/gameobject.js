GameObject = function (id, params, state, radius) {
	this.params = params;
	this.state = state;
	this.radius = radius;
	this.id = id;
}

GameObject.prototype.UpdateState = function(state) {
	this.state = state;
};

GameObject.prototype.SendState = function(socket) {
	
	
	socket.emit("state", {"type" : this.type, "id" : this.id, "state" : this.state} );

};


GameObject.prototype.Update = function(delta) {
	
};


