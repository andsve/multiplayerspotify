var ammo_list = [ { spotifyuri : 'spotify:track:4V9nhxmKECytODLDFztJc2', trackname : 'Chick Corea Elektric Band - Charged Particles', speed : 70, strength : 52}, { spotifyuri : 'spotify:track:4zGGxHeubHJvuGAGC287II', trackname : 'Emerson, Lake &amp; Powell - Touch and Go', speed : 98, strength : 58}, { spotifyuri : 'spotify:track:4hDf0qX5LzI87RiSyIZNYO', trackname : 'Europe - More Than Meets The Eye', speed : 100, strength : 51}, { spotifyuri : 'spotify:track:3SgZW5ULGKYHt4R2JxjxFd', trackname : 'Imperial State Electric - Sheltered In The Sand', speed : 40, strength : 40}, { spotifyuri : 'spotify:track:3nKPkmE7MLXSgySaIFvuhV', trackname : 'Triumph - Spellbound', speed : 58, strength : 24}, { spotifyuri : 'spotify:track:2sKCopgrCB5zxQXswBFSxS', trackname : 'Mitch Murder - Mitch Murder - Hollywood Heights', speed : 78, strength : 49}, { spotifyuri : 'spotify:track:1PUb3mW9ZHdY9kxE7yXlA5', trackname : 'Yngwie Malmsteen - Crystal Ball', speed : 100, strength : 61}, { spotifyuri : 'spotify:track:1nxaSsk75Gyzvd8Y7J901Z', trackname : 'Europe - Got Your Mind In The Gutter', speed : 82, strength : 34}, { spotifyuri : 'spotify:track:0WVLMmdTWQNKjT05OGHnDG', trackname : 'Greg X. Volz - Heaven Is Within You', speed : 54, strength : 31}, { spotifyuri : 'spotify:track:07oZaeCeltledc6GtxzpMV', trackname : 'H.E.A.T - In And Out of Trouble', speed : 40, strength : 40}];


Player = function (id, params, state, geometry) {
	this.mesh        = new PXF.MeshObject(id,null,geometry);
	this.type        = "player";
	this.localPlayer = false;
	this.shotsFired = 0;
	this.invincible = true;
	this.invisTime = 1.5;

	this.trail = new PXF.LineBatch(gl, "lighttrail");
	this.pos_buffer  = [];
	this.pos_max_size = 60;
	this.pos_pointer = 0;

	// Set up stuff for drawing engine flame
	var quadVertices = [  0,      5.46,  -4.729
					   , -3.485, 1.999, -4.328
					   ,  3.485,  1.999, -4.328
					   ,  0,     -1.463, -3.927];

	var uvCoords = [ 0.656, 0.351562
				   , 0.656, 0.0
				   , 1.0,      0.351562
				   , 1.0,      0.0];


	this.engineFlame = new PXF.PrimitiveBatch(gl);
	this.engineFlame.primitiveType = gl.TRIANGLE_STRIP;
	this.engineFlame.buffers["position"] = {size : 3, format : gl.FLOAT, data : quadVertices, bind : true};
	this.engineFlame.buffers["uv"] = {size : 2, format : gl.FLOAT, data : uvCoords, bind : true};
	this.engineFlame.BuildBuffers(true);
	this.engineFlame.drawLength = 4;

    var arrowVertices = [
             1.0,  1.0,  0.0,
            -1.0,  1.0,  0.0,
             1.0, -1.0,  0.0,
            -1.0, -1.0,  0.0
        ];
	var arrowUV = [
          0.0, 0.0,
          1.0, 0.0,
          1.0, 1.0,
          0.0, 1.0

	];

	this.arrow = new PXF.PrimitiveBatch(gl);
	this.arrow.primitiveType = gl.TRIANGLE_STRIP;
	this.arrow.buffers["position"] = {size : 3, format : gl.FLOAT, data : arrowVertices, bind : true};
	this.arrow.buffers["uv"] = {size : 2, format : gl.FLOAT, data : arrowUV, bind : true};
	this.arrow.BuildBuffers(true);
	this.arrow.drawLength = 4;


	GameObject.call( this, id, params, state, this.mesh.geometry.radius * 0.75 );


	// Create name tag texture
	var text = id;
	var canvas = document.createElement('canvas');
	var c = canvas.getContext('2d');
	var size = 16;
	c.font = String(size) + "pt Arial";

	canvas.width = c.measureText(text).width;// + 8 + outline * 2; // Margin
	canvas.height = size;
	c.font = String(size) + "pt Arial";

	c.fillStyle = "white";
	c.textBaseline = "middle";
	c.textAlign = "left";

	c.fillText(text, 0, size/2);

	this.nameTexture = new PXF.Texture(gl);
	this.nameTexture.CreateFromEmpty(canvas.width, canvas.height);
	gl.bindTexture(gl.TEXTURE_2D, this.nameTexture.texture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, canvas);
}

Player.prototype = new GameObject();
Player.prototype.constructor = Player;

Player.prototype.GetTransform = function() {
	return mat4.create(this.mesh.WorldTransform);
}

Player.prototype.Render = function(shader,bindBuffers) {
	//this.drawTrail(shader);

	shader.SetUniform("mat4","uOBJMatrix",this.mesh.WorldTransform);
	shader.SetUniform("f3", "color", this.params.drawColor);
	shader.SetUniform("f", "damage", this.state.health / 5.0);
	shader.SetUniform("i", "state", 0);
	shader.SetUniform("f", "health", this.state.health / 5 );

	this.mesh.Draw(shader,bindBuffers);
	shader.SetUniform("f", "damage",1);
};

Player.prototype.drawTrail = function (shader) {
	// Draw light trail
	this.trail.Reset();
	this.trail.buffers["uv"] = {size : 2, format : gl.FLOAT, data : [], bind : true};

	var prevPos = this.state.pos;
	var n = this.pos_max_size;
	for (var i in this.pos_buffer) {
		this.trail.Add(prevPos, this.pos_buffer[i]);
		this.trail.buffers["uv"].data.push(n/this.pos_max_size);
		this.trail.buffers["uv"].data.push(n/this.pos_max_size);
		this.trail.buffers["uv"].data.push(n/this.pos_max_size);
		this.trail.buffers["uv"].data.push(n/this.pos_max_size);
		this.trail.buffers["uv"].data.push(n/this.pos_max_size);
		prevPos = this.pos_buffer[i];
		n--;
	}
	this.trail.End();

	var identityMat = mat4.identity();

	shader.SetUniform("i", "state", 3);
	shader.SetUniform("f3", "color",this.params.drawColor);

	gl.enable(gl.BLEND);
	this.trail.primitiveType = gl.LINE_STRIP;
	this.trail.drawLength = this.pos_buffer.length * 2;

	this.trail.BindBuffers(shader, {position: true, normal: false, uv: true, tex0: false});

	shader.SetUniform("mat4", "uOBJMatrix", identityMat);
	this.trail.DrawBuffers();

	this.trail.UnbindBuffers(shader, {position: true, normal: false, uv: true, tex0: false});
	gl.disable(gl.BLEND);
};

Player.prototype.drawEngine = function (shader) {
		// Draw engine flame
	gl.enable(gl.BLEND);
	shader.SetUniform("mat4","uOBJMatrix",this.mesh.WorldTransform);
	shader.SetUniform("i", "state", 4);
	shader.SetUniform("i", "boost", this.state.boosting ? 1 : 0);
	this.engineFlame.BindBuffers(shader, {position : true, normal : false, uv: true});
	this.engineFlame.DrawBuffers();
	this.engineFlame.UnbindBuffers(shader, {position : true, normal : false, uv: true});
	gl.disable(gl.BLEND);
};

Player.prototype.drawArrow = function (shader) {
	var pos = vec3.create(this.state.pos);
	var mtx = mat4.create();
	var p = vec3.create();

	// mat4.multiplyVec3( camera.InvWorldTransform, pos, p );
	// mat4.multiplyVec3( camera.pMatrix, pos, p );

	mat4.multiply( camera.pMatrix, camera.InvWorldTransform, mtx );
	mat4.multiplyVec3( mtx, pos, p );

	pos[0] = clamp(pos[0], -1, 1);
	pos[1] = clamp(pos[1], -1, 1);

	var qb = new PXF.QuadBatch(gl);
	qb.Reset();

	if( gamestate.localPlayerObject === this )
		qb.AddCentered( p[0], p[1] - 0.15, 0.08, 0.04);
	else 
		qb.AddCentered( p[0], p[1], 0.1, 0.05);

	qb.End();

	this.nameTexture.Bind(shader,"nameTex",2);

	qb.BindBuffers(shader, { position : true, uv: true, normal: false });
	qb.DrawBuffers();
	qb.UnbindBuffers(shader,{ position : true, uv: true, normal: false });

	qb.Destroy();
};

Player.prototype._dampenRotation = function (idx, accDelta) {
	if ((this.state.drot[idx] > accDelta * 0.8) && (this.state.drot[idx] < -accDelta * 0.8)) {
		this.state.drot[idx] -= (this.state.drot[idx] > 0 ? accDelta : accDelta) * 0.8;
	}
	else {
		this.state.drot[idx] = 0.0;
	}

	if ((this.state.drot[idx] < 0.7) && (this.state.drot[idx] > -0.7)
			&& (this.state.vrot[idx] < 0.5) && (this.state.vrot[idx] > -0.5)) {
		this.state.vrot[idx] = 0;
		this.state.drot[idx] = 0;
	}

	if ((this.state.drot[idx] == 0.0) && this.state.vrot[idx] !== 0.0) {
		this.state.drot[idx] = this.state.vrot[idx] > 0 ? -accDelta : accDelta;
	}
};

Player.prototype.Update = function(delta, inputState) {
	this.invisTime-=delta;
	if (this.invisTime < 0) this.invincible = false;
	var dirtyState = false;
	
	var ta;
	if (inputState) // Tells that we are the actual player
	{
		var mousePos = vec2.createFrom(PXF._inputState.mousePos[0], PXF._inputState.mousePos[1]);
		var center = vec2.createFrom(Math.round(ctx.gl.canvas.width / 2), Math.round(ctx.gl.canvas.height / 2));
		var mouseDirection = vec2.create();
		vec2.subtract(mousePos, center, mouseDirection);

		// flip
		mouseDirection[0] = -mouseDirection[0];
		mouseDirection[1] = -mouseDirection[1];
		mouseTan = Math.atan2(mouseDirection[0], mouseDirection[1]);

		// Heading
		var heading = vec3.create([0.0, 0.0, 1.0]);
		mat4.multiplyVec3(this.state.o, heading);
		vec3.normalize(heading);
		var hd = Math.atan2(heading[0], heading[1]);


		if (Math.abs(hd) < 1.5)
		{
		}
		else {
		mouseTan = mouseTan < 0 ? mouseTan + 2* Math.PI : mouseTan;
		hd = hd < 0 ? hd + 2*Math.PI : hd;
		}

		var diff = mouseTan - hd;

		var ta = clamp(diff, -0.2, 0.2); // where we want to go :P

		//// Yaw
		if (inputState["YLEFT"] || inputState["YRIGHT"]) {
			this.state.drot[2] += inputState["YLEFT"] ? this.params.yawAccelDelta : -this.params.yawAccelDelta;
			this.state.drot[2] = clamp(this.state.drot[2], -this.params.maxYawAccel, this.params.maxYawAccel);
			if (this.state.yaw == "NO") { dirtyState = true; }
			this.state.yaw = inputState["YLEFT"] ? "LEFT" : "RIGHT";
		}
		else {
			if (this.state.yaw != "NO") { dirtyState = true; }
			this.state.yaw = "NO";
		}

		//this.state.shooting = inputState["FIRE"];
		this.state.shooting = PXF._inputState.mouseDown;
		this.state.boosting = inputState["BOOST"];
	}

	if ((this.state.yaw == "NO") && (this.state.vrot[2] != 0.0)) {
		this._dampenRotation(2, this.params.yawAccelDelta);
	}


	// Rotational velocity
	var rvdiff = vec3.create();
	vec3.scale(this.state.drot, delta, rvdiff);
	vec3.add(rvdiff, this.state.vrot, this.state.vrot);

	// Rotate
	var rotationDelta = vec3.create();
	this.state.vrot[0] = clamp(this.state.vrot[0], -this.params.maxRollVel,  this.params.maxRollVel);
	this.state.vrot[1] = clamp(this.state.vrot[1], -this.params.maxPitchVel, this.params.maxPitchVel);
	this.state.vrot[2] = clamp(this.state.vrot[2], -this.params.maxYawVel,   this.params.maxYawVel);
	vec3.scale(this.state.vrot, delta, rotationDelta);

	if (ta) mat4.rotateY(this.state.o, ta);

	// Heading
	var heading = vec3.create([0.0, 0.0, 1.0]);
	mat4.multiplyVec3(this.state.o, heading);
	vec3.normalize(heading);

	// Velocity
	if (this.state.boosting) {
		if ((this.state.boostEnergy > 0) && (this.state.boostCooldown == 0)) {
			this.state.a = this.params.maxAccel * 4;
			this.state.v += this.state.a * delta;
			this.state.v = clamp(this.state.v, 0.0, this.params.maxBoostVel);
			this.state.boostEnergy = Math.max(0, this.state.boostEnergy - delta);
			if (this.state.boostEnergy == 0) {
				this.state.boostCooldown = this.params.maxBoostEnergy;
			}
		}
		else {
			this.state.boosting = false;
		}

	}

	if (!this.state.boosting) {
		this.state.v += this.state.a * delta;
		this.state.v = clamp(this.state.v, 0.0, this.params.maxVelocity);
		this.state.a = clamp(this.state.a, 0.0, this.params.maxAccel);
		this.state.boostEnergy = Math.min(this.params.maxBoostEnergy, this.state.boostEnergy + delta);
		this.state.boostCooldown = Math.max(0, this.state.boostCooldown - delta);
	}

	// Move player
	var pdiff = vec3.create();
	vec3.scale(heading, this.state.v, pdiff);
	vec3.scale(pdiff, delta, pdiff);
	vec3.add(this.state.pos, pdiff);

	var bepa = mat4.identity();
	mat4.translate(bepa, this.state.pos, bepa);
	mat4.multiply(bepa, this.state.o, this.mesh.Transform);


	// buffer position - for light trails // TODO offset to fit engine
	var po = mat4.create(this.state.o);
	var sn = vec3.create([0,2,0]);
	mat4.multiplyVec3(po,sn);

	this.pos_buffer.unshift([this.state.pos[0] + sn[0], this.state.pos[1] + sn[1], this.state.pos[2] + sn[2]]);
	if (this.pos_buffer.length >= this.pos_max_size)
	{
		this.pos_buffer.pop();
	}

	if(inputState) {

		// local player is the only object that can fire
		if(this.state.shooting && !this.shotFired){
			this.shotFired = true;
			this.shotsFired++; // well, you know..


			var laser_track = ammo_list.shift();
			ammo_list.push(laser_track);
			var row = document.getElementById("track-"+String(laser_track.i));
			if (row)
				row.style.backgroundColor = "";
			var row = document.getElementById("track-"+String((laser_track.i+1) % ammo_list.length));
			if (row)
				row.style.backgroundColor = "green";
			
			var velocity = 200 + laser_track.speed * 4; // orig 600
			var radius = 1.5 + (laser_track.strength / 100) * 4.0; // orig 2.0

			// create a new laser state
			socket.emit("spawn", {"type" : "laser", "id" : this.id + "_laser" + this.shotsFired,
					"params" : { velocity : velocity	// velocity
						,ttl : 1		// time-to-live / distance?!
						,radius : radius	// bounding sphere radius
						,drawColor : this.params.drawColor
						,owner : this.id
						,track : laser_track
					},
					"state" : {
				pos : this.state.pos,
				o : this.state.o
			}}); 
		}

		if(this.shotFired) {
			this.state.shootTime += delta;

			if(this.state.shootTime >= this.params.shotTime) {
				this.state.shootTime = 0;
				this.shotFired = false;
			}
		}
	}
};

Player.prototype.Hit = function (hp, attacker, track) {

	var t = "spotify:track:2eJ8ij1T3cNUKiGdcUvKhy";
	if (track != undefined)
	{	
		t = track.spotifyuri;
	}

	t = models.Track.fromURI(t);
	
	if (t.starred)
		this.state.health += hp;
	else
		this.state.health -= hp;

	if (this.state.health <= 0) {
		this.Destroy(attacker, track);
	}
};

Player.prototype.Destroy = function (killer, track) {
	socket.emit("destroy", { "type" : "player", "id" : this.id, "killer" : killer, "track" : track });

	//var pl = pl || {add : function(){}};

	var t = "spotify:track:2eJ8ij1T3cNUKiGdcUvKhy#0:44";
	if (track != undefined)
	{
		t = track.spotifyuri;
	}

	pl.add(t);

	t = models.Track.fromURI(t);
	models.player.play(t);
	/*setTimeout(function() {
		models.player.position = 44.0;
		console.log("SUP!!!!!! NBOAS");
	}, 2000);*/
	
	
	//this.SpawnDebris();
};

Player.prototype.SpawnDebris = function () {
	socket.emit("spawn", {"type" : "debris", "id" : create_cool_id(),
		"params" : {
			"type" : "leftwing",
			"drawColor" : this.params.drawColor,
			"drawState" : 0
		},
		"state" : {
			"pos" : this.state.pos,
			"o"   : rand_orientation(),
			"vrot" : rand_vec(Math.PI),
			"v" : rand_vec(20.0)
		}
	});

	socket.emit("spawn", {"type" : "debris", "id" : create_cool_id(),
		"params" : {
			"type" : "leftwing",
			"drawColor" : this.params.drawColor,
			"drawState" : 0
		},
		"state" : {
			"pos" : this.state.pos,
			"o"   : rand_orientation(),
			"vrot" : rand_vec(Math.PI),
			"v" : rand_vec(20.0)
		}
	});

	socket.emit("spawn", {"type" : "debris", "id" : create_cool_id(),
		"params" : {
			"type" : "shipbody",
			"drawColor" : this.params.drawColor,
			"drawState" : 0
		},
		"state" : {
			"pos" : this.state.pos,
			"o"   : rand_orientation(),
			"vrot" : rand_vec(Math.PI),
			"v" : rand_vec(20.0)
		}
	});
};

