
var lobby_elems = {};

function initLobby()
{
	lobby_elems["holder"] = document.getElementById('lobby');
	lobby_elems["roomlist"] = document.getElementById('roomlist');
	lobby_elems["lobbyinfo"] = document.getElementById('lobbyinfo');

	showLobby();
}

function onDisconnect (msg) {
	lobby_elems["lobbyinfo"].innerHTML = "Disconnected from game server: " + msg;
	alert("Disconnected from server!");
}

function onLobbyState(data)
{

	// Repopulate room list
	lobby_elems["roomlist"].innerHTML = "";
	var c = 0;
	for (var i in data.rooms)
	{
		c += 1;
		var room = data.rooms[i];
		//var room_entry = "<li>" + room.name + " " + room.players + "/2 players - <a href='#' onclick='enterRoom(\"" + room.name + "\");'>Join</a></li>";
		var room_entry = '<span class="sp-item sp-track sp-track-availability-0" title="SUP BRO" data-itemindex="'+c+'" data-viewindex="'+c+'" style="-webkit-transform: translateY('+(c*20)+'px);"><span class="sp-track-field-name">' + room.name + '</span><span class="sp-track-field-name">' + room.players + '/16 players</span><button class="button" onclick="enterRoom(\'' + room.name + '\');" style="color: #000;">Join game...</button></span>';
		lobby_elems["roomlist"].innerHTML += room_entry;
	}
	lobby_elems["roomlist"].style.height = "" + ((c + 1) * 20) + "px";
}

function enterRoom(id)
{
	//lobby_elems["holder"].style.display = "none";
	lobby_elems["lobbyinfo"].innerHTML = "Trying to join game '" + id + "', please wait...";

	joinGame(id);

}

function hideLobby()
{
	// TODO Tween up in this bitch
	lobby_elems["holder"].style.display = "none";

}

function showLobby()
{
	lobby_elems["holder"].style.display = "block";

}