vec3.clamp = function (vec, low, high, dest) {
	var tmp = vec3.create();
	for (var i=0; i<3; i++) {
		dest[i] = Math.max(Math.min(high, vec[i]), low);		
	}

	dest = tmp;
};

clamp = function (v, low, high) {
	return Math.max(Math.min(v, high), low);
}


function create_cool_id() {
  return Math.random().toString(36).substring(8);
}

function rand_orientation () {
	var o = mat4.identity();
	mat4.rotateX(o, (Math.random()*2.0 - 1.0)*3.14);
	mat4.rotateY(o, (Math.random()*2.0 - 1.0)*3.14);
	mat4.rotateZ(o, (Math.random()*2.0 - 1.0)*3.14);

	return o;
}

function rand_vec(scale) {
	var o = [(Math.random()*2.0 - 1.0)*scale, (Math.random()*2.0 - 1.0)*scale, (Math.random()*2.0 - 1.0)*scale];

	return o;
}