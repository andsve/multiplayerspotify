Debris = function (id, params, state, geometry)
{
	this.transform 	= mat4.identity();
	this.geometry = geometry;
	this.type = "debris";

	GameObject.call( this, id, params, state, 0.0 );

	this.maxTime = 10;
	this.elapsedTime = 0;

	this.params.drawScale = this.params.drawScale || vec3.create([1,1,1]);
}

Debris.prototype = new GameObject();
Debris.prototype.constructor = Debris;


Debris.prototype.Update = function(delta) {
	
	this.maxTime -= delta;
	if(this.maxTime <= 0 && this.params.type != "asteroid") 
		return false;

	var rotationDelta = vec3.create();
	vec3.scale(this.state.vrot, delta, rotationDelta);
	mat4.rotateZ(this.state.o, rotationDelta[0]);
	mat4.rotateX(this.state.o, rotationDelta[1]);
	mat4.rotateY(this.state.o, rotationDelta[2]);

	var mpos = vec3.create(this.state.v);
	vec3.multiply(mpos, [delta, delta, delta]);
	vec3.add(this.state.pos, mpos);

	var bepa = mat4.identity();

	mat4.translate(bepa, this.state.pos, bepa);
	mat4.multiply(bepa, this.state.o, bepa);

	mat4.scale(bepa,this.params.drawScale,this.transform);

	return true;
}

Debris.prototype.Render = function(shader,bindBuffers) {
	shader.SetUniform("mat4", "uOBJMatrix", this.transform);
	shader.SetUniform("f3", "color", this.params.drawColor);

	if(this.params.drawState != undefined) 
		shader.SetUniform("i", "state", this.params.drawState);


	bindBuffers = { "position" : true, "uv" : true, "normal" : false, "tex0" : true};

	this.geometry.BindBuffers(shader, bindBuffers);
	this.geometry.DrawBuffers();
	this.geometry.UnbindBuffers(shader, bindBuffers);

	if(this.params.drawState != undefined) 
		shader.SetUniform("i", "state", 0);
}
