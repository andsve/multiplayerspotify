var run = true;
var ctx;
var sp;

function startengine()
{
    glcanvas = document.getElementById('glcanvas');
    var w = glcanvas.width;
    var h = glcanvas.height;
    
    // create pxf instance / gl context
    ctx = new PXF.Instance( w, h, document.getElementById('glcanvas'), true, true, false, true, true );
    gl  = ctx.gl;
    gl.clearColor(1.0, 0.0, 0.0, 1.0);

    requestAnimationFrame( animate );

    sp = getSpotifyApi(1);

    var models = sp.require("sp://import/scripts/api/models");
    var player = models.player;
    var playlist = models.Playlist.fromURI("spotify:user:spotify:playlist:3Yrvm5lBgnhzTYTXx2l55x", function(pl) {
        console.log(pl.name + " loaded!!");
    });

    var track = models.Track.fromURI("spotify:track:7G76UIYs6vaDD1Gm1NCSBm", function(tr) {
        console.log(tr.name + " loaded!!");
    });
}


function render()
{
    if ( run ) 
    {

        gl.viewport( 0, 0, ctx.ctxWidth, ctx.ctxHeight );
        gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );

    }

}

function animate() {
    render();
    requestAnimationFrame(animate);
}

