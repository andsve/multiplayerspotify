// Input handler, makes sure that keyboard input is valid and manages keyboard
// mapping (Input.mapping)
Input = {};

// Needed for preventing default action of keys
PXF._inputState.callbackOnUpdate = false;
Input._dummyFun = function(){};

Input.state = {"LEFT"   : false
             , "RIGHT"  : false
             , "UP"     : false
			 , "DOWN"   : false
			 , "YLEFT"  : false
			 , "YRIGHT" : false
			 , "FIRE"   : false
			 , "BOOST"  : false
};

// Key mappings - dvorak :3
Input.dvorak =  {'a' : "YLEFT"
               , 'e' : "YRIGHT"
			   , "RIGHT" : "RIGHT"
			   , "LEFT"  : "LEFT"
			   , "UP"    : "UP"
			   , "DOWN"  : "DOWN"
			   , "CTRL"  : "FIRE"
			   , "SHIFT" : "BOOST"
};

// Key mappings - qwerty :(
Input.qwerty =  {'a' : "YLEFT"
               , 'd' : "YRIGHT"
			   , "RIGHT" : "RIGHT"
			   , "LEFT"  : "LEFT"
			   , "UP"    : "UP"
			   , "DOWN"  : "DOWN"
			   , "CTRL"  : "FIRE"
			   , "SHIFT" : "BOOST"
};

Input.mapping = Input.qwerty;
PXF.registerKeyCallback(Object.keys(Input.mapping), Input._dummyFun);

Input.SetKeymap = function (keyMap) 
{
	PXF.unregisterKeyCallback(Object.keys(Input.mapping), Input._dummyFun);
	Input.mapping = keyMap;
	PXF.registerKeyCallback(Object.keys(Input.mapping), Input._dummyFun);

};

Input.Update = function() {
	for (var k in Input.mapping) {
		var down = false;
		var key = Input.mapping[k];

		if (PXF._inputState.specialKeys[k]) 
		{
			down = PXF._inputState.keyState[PXF._inputState.specialKeys[k]] 
				   && (PXF._inputState.keyState[PXF._inputState.specialKeys[k]].state == "press");
		}
		else
		{
			k = k.toUpperCase().charCodeAt();
			down = PXF._inputState.keyState[k]
				   && (PXF._inputState.keyState[k].state == "press");
		}

		if (!down) 
		{
			Input.state[key] = false;
		}
		else 
		{
			// Make sure there's no illegal input. In that case just skip it..
			if ((key == "LEFT") && Input.state["RIGHT"]) { continue; }
			if ((key == "RIGHT") && Input.state["LEFT"]) { continue; }
			if ((key == "UP") && Input.state["DOWN"]) { continue; }
			if ((key == "DOWN") && Input.state["UP"]) { continue; }
			if ((key == "YLEFT") && Input.state["YRIGHT"]) { continue; }
			if ((key == "YRIGHT") && Input.state["YLEFT"]) { continue; }

			Input.state[key] = true;
		}
	}
};

