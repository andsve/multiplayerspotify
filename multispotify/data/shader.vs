attribute vec3 position;
attribute vec3 normal;
attribute vec2 uv;

uniform mat4 uPMatrix;
uniform mat4 uMVMatrix;
uniform mat4 uNMatrix;
uniform mat4 uOBJMatrix;

uniform int state;

varying vec2 uv0;
varying vec4 n;
varying float lazer_alpha;
varying vec3 pos;

const float max_y = 20.0;

void main(void) {
	n = uNMatrix * vec4(normal, 0.0);
	uv0 = uv;
	pos = position;

    gl_Position = uPMatrix * uMVMatrix * uOBJMatrix * vec4( position, 1.0);


    if(state == 1) {
		lazer_alpha  = (position.z + max_y) / (max_y * 2.0);
	} 
}
