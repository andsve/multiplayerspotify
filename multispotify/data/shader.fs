precision mediump float;

varying vec2 uv0;
varying vec4 n;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D nameTex;

uniform vec3 color;
uniform int state;
uniform float lazer_time;
uniform int boost;
uniform float damage;
varying float lazer_alpha;
varying vec3 pos;

void main(void) {
	
	if(state == 0) { // player object
		vec4 sample = texture2D(tex0, uv0.st);

		if(sample.a == 0.0) {
			gl_FragColor = vec4(color * sample.r, 1.0);
		} else {
			gl_FragColor = vec4(sample.rgb, 1.0);
		}
		gl_FragColor.r = mix(1.0, gl_FragColor.r, damage);

	} else if (state == 1) { // lazer blast
		float a = 0.9;
		if(lazer_time > 0.7)
			a = 1.0 - lazer_time;
		else 
			a = lazer_alpha;

		gl_FragColor = vec4(color.rgb+n.xyz*0.00001,a);
		
	} else if (state == 3) { // light trail
		gl_FragColor = vec4(color.rgb, uv0.s);
		// Cooler stuffs!

	} else if (state == 4) { // engine flame
		vec4 sample = texture2D(tex0, uv0.st);
		if (boost == 1) {
			sample.a *= 1.5;
			sample.rgb += vec3(0.2, 0.2, 0.2);
		}
		gl_FragColor = sample;

	} else if(state==5) { // hemoroids
		vec4 sample = texture2D(tex1,uv0.st);	

		gl_FragColor = vec4( sample.rgb , 1.0);
	} else if(state==6) { // name arrow
		vec4 col = texture2D(nameTex, uv0.st);
		gl_FragColor = vec4(col.rgb, 0.8);
	}
}
