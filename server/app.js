var app = require('http').createServer(handler)
  , io = require('socket.io').listen(app)
  , fs = require('fs')
  , path = require('path');

app.listen(8080);

/************************************************************
 * HTTP server handling function.
 */ 
function handler (req, res) {

}

function create_player_id() {
  return Math.random().toString(36).substring(8);
}

/************************************************************
 * Game server stuffs
 */
var max_players_per_room = 16;
var max_space_size = 30.0;

function createGameSession(id)
{
  var session = { "id" : id,
                  "players" : {},
                  "player_count" : 0,
                  "scoreboard" : {},
                  "gameobjects" : {} };


  return session;
}

function createPlayer() {

  var params = {
      maxVelocity : 100.0
	  , maxBoostVel : 180.0
    , maxAccel    : 20.0

    , maxRollVel  : 3.1415
    , maxPitchVel : 4.5
    , maxYawVel   : 12.5

    , maxRollAccel   : 8.0
    , maxPitchAccel  : 10.0
    , maxYawAccel    : 5.0

    , rollAccelDelta  : 8.0
    , pitchAccelDelta : 12.0
    , yawAccelDelta   : 23.0

    , rotAccelDamp : 1000.0
	  , maxHealth : 10.0
    , shotTime  : 0.33      // Delay between shots fired
    , drawColor : [Math.random(),Math.random(),Math.random()]
	, maxBoostEnergy : 3.0
  };
  
  var state = {   pos : [0.0, 50.0, 0.0] // Position
			         	, v : 100.0               // Heading velocity
        				, a : 30.0              // Heading acceleration
				, o : [1, 0, 0, 0, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1] // -90° around x
				//, o : [1, 0, 0, 0, 0, -3.205103515924179e-9, 1, 0, 0, -1, -3.205103515924179e-9, 0, 0, 0, 0, 1] // 90° around x
        //, o : [1,0,0,0,
        //       0,1,0,0,
        //       0,0,1,0,
        //       0,0,0,1]// Orientation
        , vrot : [0.0, 0.0, 0.0] // Rotational velocity
        , drot : [0.0, 0.0, 0.0] // Rotational acceleration

        // State
				, health : 5.0
        , rolling : "STRAIGHT" // LEFT / RIGHT / STRAIGHT  - If ship is rolling
        , shooting: false
        , shootTime : 0.0  // Time since last shot
        , boosting  : false
				, boostEnergy : 3.0
				, boostCooldown : 0.0
              };

  state.pos[0] = Math.random() * max_space_size;
  state.pos[1] = Math.random() * max_space_size;
  //state.pos[2] = Math.random() * max_space_size;

  return {"params" : params, "state" : state};
}


/***********************************************************
 * Networking logic
 */
var game_counter = 0;
var game_sessions = {};

sockserv = io.on('connection', function (socket) {

    /////////////////////////////////////////////////////////
    // a player has disconnected
    socket.on('disconnect', function () {
      // make sure the player leaves the game
      var prevroom;
      socket.get('roomid', function(err, roomid) { prevroom = roomid; });
      if (prevroom != undefined)
      {
        console.log("Player leaving room " + prevroom);
        game_sessions[prevroom].player_count -= 1;

		var playerid;
        socket.get('playerid', function(err, roomid) { playerid = roomid; });

		delete game_sessions[prevroom].players[playerid];
		delete game_sessions[prevroom].scoreboard[playerid];

        // destroy room if this was the last player to leave
        if (game_sessions[prevroom].player_count <= 0)
        {
          delete game_sessions[prevroom];
        } else {

          // let everyone know that the player left
          socket.broadcast.to(prevroom).emit('game', "someone left the game lol");
        }

        // leave game room
        socket.leave(prevroom);
      }

    });

    /////////////////////////////////////////////////////////
    // a player sends state update that needs to be broadcasted
    socket.on('state', function(state) {
      
      // check if we need to leave a previous game first
      var game;
      socket.get('roomid', function(err, roomid) { game = roomid; });
      if (game == undefined)
      {
        socket.disconnect("GET OUT OF HERE SON");
        return;
      }

      // check if game session exist
      if (game in game_sessions)
      {

        // get current session
        var session = game_sessions[game];

        // TODO Only broadcast if we need to!
        // broadcast state update
        socket.in(game).broadcast.emit('state', state);
        
      } else {
        // Tried to enter a session that was not created, kick the user, WHAT A DOUCHE!
        socket.disconnect("SERVED MISTR!!!");
      }

    });

    /////////////////////////////////////////////////////////
    // a player sends spawn request that needs to be broadcasted
    socket.on('spawn', function(spawnreq) {

      // make sure we are in the correct room
      var game;
      socket.get('roomid', function(err, roomid) { game = roomid; });
      if (game == undefined)
      {
        socket.disconnect("GET OUT OF HERE SON");
        return;
      }

      // check if game session exist
      if (game in game_sessions)
      {



        // get current session
        var session = game_sessions[game];
        if ("keep" in spawnreq && spawnreq.keep)
        {
          // store stuff on server
          session.gameobjects[spawnreq.id] = spawnreq;
        }

        socket.in(game).broadcast.emit('spawn', spawnreq);

        socket.in(game).emit('spawn', spawnreq);
        
      } else {
        // Tried to enter a session that was not created, kick the user, WHAT A DOUCHE!
        socket.disconnect("SERVED MISTR!!!");
      }

    });

    /////////////////////////////////////////////////////////
    // a player sends destroy request that needs to be broadcasted
    socket.on('destroy', function(destroyreq) {

      // make sure we are in the correct room
      var game;
      socket.get('roomid', function(err, roomid) { game = roomid; });
      if (game == undefined)
      {
        socket.disconnect("GET OUT OF HERE SON");
        return;
      }

      // check if game session exist
      if (game in game_sessions)
      {

        // get current session
        var session = game_sessions[game];
        socket.in(game).broadcast.emit('destroy', destroyreq);
        socket.in(game).emit('destroy', destroyreq);

		if (destroyreq["killer"]) {
			var player;
			socket.get('playerid', function(err, plid) { player = plid; });
      session.scoreboard[player].deaths += 1;
			session.scoreboard[player].username = session.players[player].state.username;
			session.scoreboard[destroyreq["killer"]].kills += 1;

			socket.in(game).emit('game', { "event" : "scoreboard",  "scoreboard" : session.scoreboard});
			socket.in(game).broadcast.emit('game', { "event" : "scoreboard",  "scoreboard" : session.scoreboard});
		}
        
      } else {
        // Tried to enter a session that was not created, kick the user, WHAT A DOUCHE!
        socket.disconnect("SERVED MISTR!!!");
      }

    });

    /////////////////////////////////////////////////////////
    // rename a player
    /*socket.on('rename', function(newname) {

      var game;
      var playerid;
      socket.get('roomid', function(err, roomid) { game = roomid; });
      socket.get('playerid', function(err, roomid) { playerid = roomid; });


      if (game == undefined)
      {
        return;
      }

      // check if game session exist
      if (game in game_sessions)
      {
        // propagate user name change
        if (playerid in game_sessions[game].players)
        {
          game_sessions[game].players[playerid].params.username = newname;
        }
      }

    });*/

    /////////////////////////////////////////////////////////
    // a player wants to join a game
    socket.on('join', function(game) {
      
      // check if game session exist
      if (game in game_sessions)
      {

        // get]current session
        var session = game_sessions[game];

        // check if we need to leave a previous game first
        var prevroom;
        socket.get('roomid', function(err, roomid) { prevroom = roomid; });
        if (prevroom != undefined)
        {
          console.log("Player leaving room " + prevroom);
          game_sessions[prevroom].player_count -= 1;
          socket.leave(prevroom);
        }

        // Create player id
        var playerid = create_player_id();
        socket.set('playerid', playerid, function () {});

        if (session.player_count < max_players_per_room)
        {
          // is host?
          if (session.player_count == 0)
          {
            socket.in(game).emit("game", {"event" : "UHOST", "reason" : "TIME TO HOST!"});
          }

          // add player to game session, and join specific socket.io "room"
          session.player_count += 1;
          socket.join(game);
          socket.set('roomid', game, function () {  });

          // player successfully joined game! LETS PLAY SOME BALL
          socket.in(game).emit("game", {"event" : "welcome", "reason" : "Cool story bro!"});

          // spawn a new player object and state
          session.players[playerid] = createPlayer();
		      session.scoreboard[playerid] = {"kills" : 0, "deaths" : 0, "username" : playerid};
          socket.in(game).emit("game", {"event" : "playerid", "playerid" : playerid } );

          // Send the new player all old player states
          for (var player in session.players)
          {
            socket.in(game).emit("spawn", {"type" : "player", "playerid" : player, "params" : session.players[player].params, "state" : session.players[player].state });
          }

          // send all keeped objects, lol
          for (var kobj in session.gameobjects)
          {
            socket.in(game).emit('spawn', session.gameobjects[kobj]);
          }

          // Send all players the new players state
          socket.in(game).broadcast.emit("spawn", {"type" : "player", "playerid" : playerid, "params" : session.players[player].params, "state" : session.players[player].state });

		  // Send scoreboard...  to..  EVERYONE! \o/
          socket.in(game).emit('game', { "event" : "scoreboard",  "scoreboard" : session.scoreboard});
          socket.in(game).broadcast.emit('game', { "event" : "scoreboard",  "scoreboard" : session.scoreboard});

        } else {

          // no room for you *okay*
          socket.in(game).emit("game", {"event" : "kicked", "reason" : "Max players reached!"});
        }
        
      } else {
        // Tried to enter a session that was not created, kick the user, WHAT A DOUCHE!
        socket.disconnect("SERVED MISTR!!!");
      }

    });

    /////////////////////////////////////////////////////////
    // stuff to do as soon as a player connected to the "lobby"

    // temp, create a new game session each time someone joins
    if (game_counter < 5)
    {
      var new_game_name = "game" + game_counter;
      game_counter += 1;
      game_sessions[new_game_name] = createGameSession(new_game_name);
    }

    // Send all game sessions so everyone can update the game list
    var gamelist = [];
    for (var i in game_sessions)
    {
      gamelist.push({"name" : i, "players" : game_sessions[i].player_count});
    }
    socket.emit('lobbychange', {'rooms' : gamelist})
    socket.broadcast.emit('lobbychange', {'rooms' : gamelist})
  });
