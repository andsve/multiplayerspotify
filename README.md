# Battlefy

## A multiplayer-music-discovery-space-shooter-prototype-game in Spotify Apps.
This is the repo for our ([pixelfolders](http://www.pixelfolders.se)) music hack entry in *Way Out West Hackathon 2012*.
For more information about both the hackathon an our entry, [click here](http://www.pixelfolders.se/gamedev/WOWHack-2012.html).

## Disclaimer
**Warning:** This is a hack, upon a hack, highly unstable and with code quality beyond this planet. We take **no responsability** for what this might do to you, your computer and your software. Please proceed with caution! **Again, we can't stress this enough: this is a hack upon a hack, code quality is extremely bad**.

Also, the current Spotify Apps version of Battlefy **will not run under Windows**, due to the lack of WebGL support in current Windows builds of Spotify.

### License
[GPL](http://www.gnu.org/licenses/gpl.html)


## Getting the source
* *As a zip file:* [https://bitbucket.org/andsve/multiplayerspotify/get/master.zip](https://bitbucket.org/andsve/multiplayerspotify/get/master.zip)
* *Cloning Git repo:* `git clone git@bitbucket.org:andsve/multiplayerspotify.git`

## Getting the client running inside Spotify Apps
1. You need to have a **Spotify Developer Account**. This is free and does **not** require Premium membership, but comes with its own set of *Terms of Use* etc. Follow these instructions: [clickity-click](https://developer.spotify.com/technologies/apps/#developer-account).
2. The source include a `multispotify` directory, this should be linked or copied into your Spotify Apps folder:
    * *Under Windows:* `My Documents\Spotify`
    * *Under Mac OS X and Linux:* `~/Spotify`
    (If the `Spotify` folder does not exist, just create it.)
3. Open Spotify and make sure you have a `Develop` menu item (i.e. Spotify is successfully indentifying you as a Developer Account).
4. In the search bar, enter: `spotify:app:battlefy` and press enter.
5. If everything has worked, the game will start within a couple of seconds.

## Getting the client running inside a browser (for non-OSX or non-Spotify users)
To start the web client, just open the multispotify/game.html in a web browser and hope that it works. We cannot guarantee that it will work in any browser other than Chrome. 

The web client is setup to connect to our public game server. If the client is unable to connect to this server (*remember: unstable / hacky code!*), or if you would like to test against your own server, you will have to open game.html in a text-editor that suits your flavor and change ..

**socket = io.connect( your-host );**

.. to whatever the server address is.

## Running the server
1. You need [node.js](http://nodejs.org/) and Socket.io (`npm install socket.io` inside `server` directory in source)
2. Inside `server` directory, run: `node app.js`
